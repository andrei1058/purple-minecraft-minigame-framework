package com.andrei1058.purplemc.worldmanager;

import com.andrei1058.purplemc.arenamanager.PurpleArena;
import com.andrei1058.purplemc.server.SetupSession;

import java.nio.file.Path;
import java.util.List;

/**
 * This is used to tell the engine how to reset maps.
 * To register your own adapter use {@link com.andrei1058.purplemc.arenamanager.ArenaManager#setMapAdapter(MapAdapter)}
 */
public interface MapAdapter {

    /**
     * Load the arena world.
     * This is called after doing {@link com.andrei1058.purplemc.arenamanager.EnableConditions} checks when creating a new {@link PurpleArena} instance.
     * The arena will be declared enabled automatically based on WorldLoadEvent.
     *
     * @param arena arena.
     */
    void onEnable(PurpleArena arena);

    /**
     * This is typically called when the arena is disabled via command.
     *
     * @param arena arena.
     */
    void onDisable(PurpleArena arena);

    /**
     * Restore the world.
     * call new {@link PurpleArena} when it's done.
     *
     * @param arena arena.
     */
    void onRestart(PurpleArena arena);

    /**
     * Load the world for setting it up.
     *
     * @param setupSession setup session.
     */
    void onSetupSessionStart(SetupSession setupSession);

    /**
     * Unload the world.
     *
     * @param setupSession setup session.
     */
    void onSetupSessionClose(SetupSession setupSession);

    /**
     * Remove lobby blocks.
     *
     * @param arena arena.
     */
    void onLobbyRemoval(PurpleArena arena);

    /**
     * Check if given world exists.
     *
     * @param name world name.
     */
    boolean isWorld(String name);

    /**
     * Delete a world.
     *
     * @param name world name.
     */
    void deleteWorld(String name);

    /**
     * Clone an arena world.
     *
     * @param name1 world to be copied.
     * @param name2 new world name.
     */
    void cloneArena(String name1, String name2);


    /**
     * Get worlds list in world container.
     *
     * @return world list in container.
     */
    List<String> getWorldsInContainer();

    /**
     * Convert wold at given path.
     *
     * @param path target world.
     */
    void convertWorld(Path path);
}
