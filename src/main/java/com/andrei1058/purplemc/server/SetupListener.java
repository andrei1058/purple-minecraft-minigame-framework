package com.andrei1058.purplemc.server;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class SetupListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerLeave(PlayerQuitEvent e){
        if (SetupSession.isInSetupSession(e.getPlayer())){
            SetupSession ss = SetupSession.getSession(e.getPlayer());
            if (ss != null) ss.close();
        }
    }
}
