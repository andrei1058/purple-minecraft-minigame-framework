package com.andrei1058.purplemc.server;

/**
 * All servers can be cross-connected if bungeecord is set to true.
 * For example you can have {@link ServerType#BUNGEE_SCALABLE} arenas listed in a {@link ServerType#MULTI_ARENA_LEGACY} selector GUI.
 */
public enum ServerType {
    /**
     * This will use a server instance as an arena.
     */
    BUNGEE_LEGACY,
    /**
     * This will use a server instance for multiple arenas an a lobby.
     */
    MULTI_ARENA_LEGACY,
    /**
     * This will use a server instance for multiple arenas.
     * This will automatically clone and load arenas when required.
     * Limitations can be configured.
     */
    BUNGEE_SCALABLE,
    /**
     * This will use a server instance for multiple arenas and a lobby.
     * This will automatically clone and load arenas when required.
     * Limitations can be configured.
     */
    MULTI_ARENA_SCALABLE,
}
