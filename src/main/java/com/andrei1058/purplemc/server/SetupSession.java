package com.andrei1058.purplemc.server;

import com.andrei1058.purplemc.PurpleMC;
import com.andrei1058.purplemc.arenamanager.ArenaManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.LinkedList;

/**
 * Used when creating or editing arenas.
 */
public class SetupSession {

    private Player player;
    private String worldName;

    private static LinkedList<SetupSession> setupSessions = new LinkedList<>();

    /**
     * Create a new setup session.
     *
     * @param player target player.
     * @param world  target world name.
     */
    public SetupSession(Player player, String world) throws SetupSessionException {
        this.player = player;
        this.worldName = world;
        if (!world.toLowerCase().equals(world))
            throw new SetupSessionException(SetupSessionException.Type.WORLD_NAME_BAD_SYNTAX);
        if (!PurpleMC.getMapAdapter().isWorld(world))
            throw new SetupSessionException(SetupSessionException.Type.UNKNOWN_WORLD);
        if (ArenaManager.getInstance().isInEnableQueue(world))
            throw new SetupSessionException(SetupSessionException.Type.ARENA_NOT_DISABLED);
        if (Bukkit.getWorld(world) != null)
            throw new SetupSessionException(SetupSessionException.Type.WORLD_ALREADY_LOADED);

        PurpleMC.getMapAdapter().onSetupSessionStart(this);
        setupSessions.add(this);
    }

    /**
     * @return player.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * @return world name.
     */
    public String getWorldName() {
        return worldName;
    }

    /**
     * When a setup session is closed.
     * This is automatically called when a player logs out.
     */
    public void close() {
        PurpleMC.getMapAdapter().onSetupSessionClose(this);
        setupSessions.remove(this);
    }

    /**
     * @param player target player.
     * @return true if target player is in a setup session.
     */
    public static boolean isInSetupSession(Player player) {
        for (SetupSession ss : setupSessions) {
            if (ss.getPlayer().getUniqueId().equals(player.getUniqueId())) return true;
        }
        return false;
    }

    /**
     * @param world target player.
     * @return true if target world is in a setup session.
     */
    public static boolean isInSetupSession(String world) {
        for (SetupSession ss : setupSessions) {
            if (ss.getWorldName().equalsIgnoreCase(world)) return true;
        }
        return false;
    }

    /**
     * @param player target player.
     * @return setup session by player.
     */
    public static SetupSession getSession(Player player) {
        for (SetupSession ss : setupSessions) {
            if (ss.getPlayer().getUniqueId().equals(player.getUniqueId())) return ss;
        }
        return null;
    }

    public static class SetupSessionException extends Throwable {

        private Type type;

        /**
         * Create a new setup session initialize error.
         *
         * @param type error type.
         */
        public SetupSessionException(Type type) {
            this.type = type;
        }

        /**
         * Get issue type.
         *
         * @return type.
         */
        public Type getType() {
            return type;
        }

        enum Type {
            /**
             * When the world is not found.
             */
            UNKNOWN_WORLD,
            /**
             * When the world is in use but not by an arena.
             */
            WORLD_ALREADY_LOADED,
            /**
             * When the world is in use by an arena and it was not disabled.
             */
            ARENA_NOT_DISABLED,
            /**
             * Thrown when the world name has a bad syntax.
             * It needs to be lower case in order to be compatible with all mc versions.
             */
            WORLD_NAME_BAD_SYNTAX
        }
    }
}
