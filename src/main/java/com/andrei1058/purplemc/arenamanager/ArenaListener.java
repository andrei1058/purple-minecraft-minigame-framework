package com.andrei1058.purplemc.arenamanager;

import com.andrei1058.purplemc.PurpleMC;
import com.andrei1058.purplemc.server.ServerType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.world.WorldLoadEvent;

public class ArenaListener implements Listener {

    /*
     * Initialize an arena if the world was loaded.
     */
    @EventHandler
    public void onWorldLoad(WorldLoadEvent e) {
        if (ArenaManager.getInstance().isInEnableQueue(e.getWorld().getName())) {
            PurpleArena pa = ArenaManager.getInstance().getFromEnablingQueue(e.getWorld().getName());
            if (pa != null) pa.init(e.getWorld());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onQuit(PlayerQuitEvent e) {
        PurpleArena pa = ArenaManager.getArenaByPlayer(e.getPlayer());
        if (pa != null) {
            pa.removePlayer(e.getPlayer());
            pa.removeSpectator(e.getPlayer());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onRespawn(PlayerRespawnEvent e) {
        PurpleArena pa = ArenaManager.getArenaByPlayer(e.getPlayer());
        if (pa != null) {
            e.setRespawnLocation(pa.onRespawnPlayerOrSpectator(e.getPlayer()));
        } else if (PurpleMC.getServerType() == ServerType.MULTI_ARENA_LEGACY || PurpleMC.getServerType() == ServerType.MULTI_ARENA_SCALABLE) {
            //todo may be main lobby
        }
    }
}
