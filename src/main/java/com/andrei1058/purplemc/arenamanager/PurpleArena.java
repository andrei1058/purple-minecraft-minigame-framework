package com.andrei1058.purplemc.arenamanager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.util.Collections;
import java.util.LinkedList;
import java.util.UUID;
import java.util.logging.Level;

public abstract class PurpleArena {

    private String displayName, worldName, arenaName;
    private World world;
    private String gameID;
    private ArenaStatus status = ArenaStatus.RESTARTING;
    private Location spectatorLocation = null, waitingLocation = null;

    private LinkedList<UUID> players = new LinkedList<>();
    private LinkedList<UUID> spectators = new LinkedList<>();
    private LinkedList<PurpleTeam> teams = new LinkedList<>();

    private int maxPlayers = 2, minPlayers = 2;

    /**
     * Create a new arena.
     *
     * @param arenaName        - parent world name.
     * @param enableConditions - check if contains all required configuration.
     */
    public PurpleArena(String arenaName, EnableConditions enableConditions) throws ArenaEnableException {
        this.displayName = arenaName;
        this.arenaName = arenaName;
        this.gameID = ArenaManager.getInstance().generateGameID();
        this.worldName = gameID;
        if (!worldName.toLowerCase().equals(worldName))
            throw new ArenaEnableException(this, worldName + " has a bad name. Do not use capital letters.", ArenaEnableException.Type.WORLD_NAME_BAD_SYNTAX);
        if (Bukkit.getWorld(worldName) != null)
            throw new ArenaEnableException(this, worldName + " is already enabled!", ArenaEnableException.Type.WORLD_ALREADY_LOADED);
        if (ArenaManager.getInstance().isArenaEnabled(this))
            throw new ArenaEnableException(this, worldName + " is already enabled!", ArenaEnableException.Type.ARENA_ALREADY_ENABLED);
        if (!enableConditions.checkConditions(this)) {
            for (String msg : enableConditions.getMessages()) {
                Bukkit.getLogger().log(Level.WARNING, "[" + worldName + "]" + msg);
            }
            throw new ArenaEnableException(this, "Missing arena data.", ArenaEnableException.Type.CONDITIONS_NOT_SATISFIED);
        }

        ArenaManager.getInstance().getMapAdapter().onEnable(this);
    }

    /**
     * Get arena world name.
     *
     * @return world name.
     */
    @SuppressWarnings("WeakerAccess")
    public String getWorldName() {
        return worldName;
    }

    /**
     * @return arena display name.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Get the root world name used for this clone.
     *
     * @return arena name.
     */
    public String getArenaName() {
        return arenaName;
    }

    /**
     * Get game identifier.
     */
    public String getGameID() {
        return gameID;
    }

    /**
     * Check if same arena.
     *
     * @param obj arena instance.
     * @return true if same world.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PurpleArena)) return false;
        return ((PurpleArena) obj).worldName.equalsIgnoreCase(this.worldName);
    }

    /**
     * Initialize current arena and remove it from the enable queue.
     *
     * @param world arena world.
     */
    void init(World world) {
        if (!ArenaManager.getInstance().isInEnableQueue(this)) return;
        this.world = world;
        ArenaManager.getInstance().removeFromEnableQueue(this);

        status = ArenaStatus.WAITING;
        ArenaManager.getInstance().addToArenasList(this);
    }

    /**
     * Get arena world.
     *
     * @return null if it was not initialized yet.
     */
    public World getWorld() {
        return world;
    }

    public boolean addPlayer(Player player) {
        if (players.contains(player.getUniqueId())) return false;
        if (players.size() < maxPlayers) {
            players.add(player.getUniqueId());
            if (players.size() >= minPlayers){
                //todo
            }
            return true;
        }
        return false;
    }

    public boolean addSpectator(Player player) {
        if (spectators.contains(player.getUniqueId())) return false;
        //todo
        spectators.add(player.getUniqueId());
        return false;
    }

    public boolean reJoinPlayer(Player player) {
        if (players.contains(player.getUniqueId())) return false;
        //todo
        players.add(player.getUniqueId());
        return false;
    }

    public void eliminatePlayer(Player player) {
        if (!players.contains(player.getUniqueId())) return;
        players.remove(player.getUniqueId());
        //todo when a player was eliminated and needs to be added as spectator
    }

    public void removePlayer(Player player) {
        if (!players.contains(player.getUniqueId())) return;
        players.remove(player.getUniqueId());
        //todo
    }

    public void removeSpectator(Player player) {
        if (!spectators.contains(player.getUniqueId())) return;
        spectators.remove(player.getUniqueId());
        //todo
    }

    /**
     * Get a player team.
     *
     * @param player target player.
     * @return null if the player is not in a team.
     */
    public PurpleTeam getPlayerTeam(Player player) {
        for (PurpleTeam pt : teams) {
            if (pt.isMember(player)) return pt;
        }
        return null;
    }

    /**
     * Get the team where a player have played.
     *
     * @param player target player.
     * @return null if the player has not played in a team.
     */
    public PurpleTeam getPlayerExTeam(Player player) {
        for (PurpleTeam pt : teams) {
            if (pt.wasMember(player)) return pt;
        }
        return null;
    }

    /**
     * @param player target player.
     * @return true if the player is in a team.
     */
    public boolean isPlaying(Player player) {
        return players.contains(player.getUniqueId());
    }

    /**
     * @param player target player.
     * @return true if the player is spectating.
     */
    public boolean isSpectating(Player player) {
        return spectators.contains(player.getUniqueId());
    }

    /**
     * Remove teams from the list.
     *
     * @param team teams to be removed.
     */
    public void removeTeam(PurpleTeam... team) {
        teams.removeAll(Collections.singleton(team));
    }

    /**
     * Add teams to the list.
     *
     * @param team teams to be added.
     */
    public void addTeam(PurpleTeam team) {
        teams.addAll(Collections.singleton(team));
    }

    /**
     * Called on player re-spawn.
     *
     * @param player target player.
     *
     * @return respawn location.
     */
    protected Location onRespawnPlayerOrSpectator(Player player) {
        if (isPlaying(player)) {
            PurpleTeam pt = getPlayerTeam(player);
            if (pt != null) {
                if (getSpectatorLocation() == null) {
                    pt.respawn(player);
                    return pt.getSpawnPoint();
                }
                pt.respawnScreen(player);
                return getSpectatorLocation();
            } else {
                //todo something bad happened, maybe waiting lobby, re-spawn
            }
        } else {
            //todo move to re spawn location or spectator location
            //todo clear inventory and give spectator inventory
        }
        return getSpectatorLocation();
    }

    /**
     * @return the arena status.
     */
    public ArenaStatus getStatus() {
        return status;
    }

    /**
     * This is used at re-spawning screen and for spectators.
     *
     * @return spectate join location.
     */
    public Location getSpectatorLocation() {
        return spectatorLocation;
    }

    /**
     * Set the location used at re-spawning screen and for spectators.
     *
     * @param spectatorLocation new location.
     */
    public void setSpectatorLocation(Location spectatorLocation) {
        this.spectatorLocation = spectatorLocation;
    }
}
