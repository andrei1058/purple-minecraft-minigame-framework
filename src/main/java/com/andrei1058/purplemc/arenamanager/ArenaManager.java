package com.andrei1058.purplemc.arenamanager;

import com.andrei1058.purplemc.worldmanager.MapAdapter;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;

public class ArenaManager {

    private static ArenaManager instance;
    private static MapAdapter mapAdapter; //todo initialize to internal adapter
    private int gid = 0;
    private String day = "", month = "";

    private ArenaManager() {
        instance = this;
    }

    private static LinkedList<PurpleArena> enableQueue = new LinkedList<>();
    private static LinkedList<PurpleArena> arenas = new LinkedList<>();
    private static HashMap<String, PurpleArena> arenaByID = new HashMap<>();
    private static HashMap<UUID, PurpleArena> arenaByPlayer = new HashMap<>();

    /**
     * Add an arena to the enable queue.
     *
     * @param arena - arena to be added.
     * @return true if the arena was added to the queue. False if is already in the queue.
     */
    public boolean addToEnableQueue(PurpleArena arena) {
        if (!isInEnableQueue(arena)) return enableQueue.add(arena);
        return false;
    }

    /**
     * Check if an arena is already in the enabling queue.
     *
     * @param arena - arena to be checked.
     * @return true if arena is already in the enabling queue.
     */
    @SuppressWarnings("WeakerAccess")
    public boolean isInEnableQueue(PurpleArena arena) {
        for (PurpleArena pa : enableQueue) {
            if (pa.equals(arena)) return true;
        }
        return false;
    }

    /**
     * Check if an arena is already in the enabling queue.
     *
     * @param arenaName - arena name to be checked.
     * @return true if arena is already in the enabling queue.
     */
    public boolean isInEnableQueue(String arenaName) {
        for (PurpleArena pa : enableQueue) {
            if (pa.getWorldName().equalsIgnoreCase(arenaName)) return true;
        }
        return false;
    }

    /**
     * Check if an arena is enabled.
     *
     * @param arena - arena to be checked.
     * @return true if the arena is enabled.
     */
    public boolean isArenaEnabled(PurpleArena arena) {
        for (PurpleArena pa : arenas) {
            if (pa.equals(arena)) return true;
        }
        return false;
    }

    /**
     * Get singleton.
     *
     * @return singleton.
     */
    public static ArenaManager getInstance() {
        return instance == null ? new ArenaManager() : instance;
    }

    /**
     * Initialize an arena.
     *
     * @param world - arena world.
     */
    protected void initializeArena(World world) {
        PurpleArena arena = arenaByID.getOrDefault(world.getName(), null);
        if (arena != null) arena.init(world);
    }

    /**
     * Remove an arena from the enable queue.
     *
     * @param arena arena to be removed.
     */
    void removeFromEnableQueue(PurpleArena arena) {
        enableQueue.remove(arena);
    }

    /**
     * Add the arena to the enabled list.
     *
     * @param arena arena to be added.
     */
    void addToArenasList(PurpleArena arena) {
        //todo
        //if (!arenaByName.containsKey(arena.getWorldName())) arenaByName.put(arena.getWorldName(), arena);
        if (!arenaByID.containsKey(arena.getGameID())) arenaByID.put(arena.getGameID(), arena);
    }

    /**
     * Set map reset adapter.
     *
     * @param mapAdapter new adapter.
     */
    public void setMapAdapter(MapAdapter mapAdapter) {
        ArenaManager.mapAdapter = mapAdapter;
    }

    /**
     * Get map adapter.
     *
     * @return map reset adapter.
     */
    public MapAdapter getMapAdapter() {
        return mapAdapter;
    }

    /**
     * Get arena instance by world name.
     *
     * @return arena instance by world name or NULL if not found.
     */
    public PurpleArena getFromEnablingQueue(String worldName) {
        for (PurpleArena pa : enableQueue) {
            if (pa.getWorldName().equals(worldName)) return pa;
        }
        return null;
    }

    public String generateGameID() {
        SimpleDateFormat y = new SimpleDateFormat("yy"), m = new SimpleDateFormat("mm"), d = new SimpleDateFormat("dd");
        String m2 = m.format(System.currentTimeMillis()), d2 = d.format(System.currentTimeMillis());
        if (!(m2.equals(this.month) || d2.equalsIgnoreCase(this.day))) {
            this.month = m2;
            this.day = d2;
            this.gid = 0;
        }
        return "y" + y.format(System.currentTimeMillis()) + "m" + this.month + "d" + this.day + "g" + gid++;
    }

    /**
     * Set arena by player. This will override existing data.
     *
     * @param player target player.
     * @param arena  target arena.
     */
    public static void setArenaByPlayer(Player player, PurpleArena arena) {
        if (arenaByPlayer.containsKey(player.getUniqueId())) {
            arenaByPlayer.replace(player.getUniqueId(), arena);
            return;
        }
        arenaByPlayer.put(player.getUniqueId(), arena);
    }

    /**
     * @return {@link PurpleArena} where the player is playing or spectating. Null if is not in arena.
     */
    public static PurpleArena getArenaByPlayer(Player player) {
        return arenaByPlayer.getOrDefault(player.getUniqueId(), null);
    }
}
