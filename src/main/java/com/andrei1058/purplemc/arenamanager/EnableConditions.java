package com.andrei1058.purplemc.arenamanager;

import java.util.ArrayList;
import java.util.List;

/**
 * Create a class that extends this class and write code to check if
 * the arena contains all required configuration in order to be enabled.
 */
public abstract class EnableConditions {

    private List<String> messages = new ArrayList<>();

    /**
     * Create a class that extends this class and write code to check if
     * the arena contains all required configuration in order to be enabled.
     * Add a error message with {@link EnableConditions#addMessage(String)} for each condition not satisfied.
     *
     * @param arena - arena to be checked.
     * @return true if all conditions are satisfied, false otherwise.
     */
    public abstract boolean checkConditions(PurpleArena arena);

    private void addMessage(String message) {
        messages.add(message);
    }

    /**
     * Get error messages to be displayed if conditions are not satisfied.
     *
     * @return list of error messages.
     */
    public List<String> getMessages() {
        return messages;
    }
}
