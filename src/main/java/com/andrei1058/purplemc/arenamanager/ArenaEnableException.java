package com.andrei1058.purplemc.arenamanager;

public class ArenaEnableException extends Throwable {

    private Type type;

    /**
     * This exception is thrown when the arena couldn't be loaded.
     * Check {@link Type} for mor details.
     */
    public ArenaEnableException(PurpleArena arena, String message, Type type) {
        super(message);
        this.type = type;
        ArenaManager.getInstance().removeFromEnableQueue(arena);
    }

    /**
     * Get exception type.
     *
     * @return exception cause.
     */
    public Type getType() {
        return type;
    }

    enum Type {
        /**
         * This exception is thrown when the world was loaded externally, when this operation is done by another plugin
         * or when the world is used at level-name in server.properties.
         */
        WORLD_ALREADY_LOADED,
        /**
         * This exception is thrown when the arena was already loaded.
         */
        ARENA_ALREADY_ENABLED,
        /**
         * This is thrown when {@link EnableConditions} are not satisfied.
         */
        CONDITIONS_NOT_SATISFIED,
        /**
         * Thrown when the world name has a bad syntax.
         */
        WORLD_NAME_BAD_SYNTAX,
    }
}
