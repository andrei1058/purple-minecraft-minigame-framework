package com.andrei1058.purplemc.arenamanager;

public enum ArenaStatus {

    WAITING, STARTING, PLAYING, RESTARTING
}
