package com.andrei1058.purplemc.arenamanager;

import com.andrei1058.purplemc.PurpleMC;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class PurpleTeam {

    //user uuid, task id
    private static HashMap<UUID, BukkitTask> reSpawningScreen = new HashMap<UUID, BukkitTask>();

    private List<UUID> teammates = new LinkedList<>();
    private List<UUID> teammatesCache = new LinkedList<>();

    private Location spawnPoint;
    private String identifier, displayName;

    /**
     * Create a team.
     *
     * @param identifier team identifier.
     * @param spawnPoint team spawn point.
     */
    public PurpleTeam(String identifier, Location spawnPoint) {
        this.identifier = identifier;
        this.spawnPoint = spawnPoint;
        this.displayName = identifier;
    }

    /**
     * Check if a player is member of the team.
     *
     * @param player target player.
     */
    public boolean isMember(Player player) {
        return teammates.contains(player.getUniqueId());
    }

    /**
     * Check if a player was a member of the team.
     * If he eventually died or if he left.
     *
     * @param player target player.
     */
    public boolean wasMember(Player player) {
        return teammatesCache.contains(player.getUniqueId());
    }

    /**
     * @return team spawn point.
     */
    public Location getSpawnPoint() {
        return spawnPoint;
    }

    /**
     * Change a team spawn location.
     *
     * @param spawnPoint new spawn location.
     */
    public void setSpawnPoint(Location spawnPoint) {
        this.spawnPoint = spawnPoint;
    }

    /**
     * Add a player to this team. Do your checks before using this method.
     * Use {@link PurpleTeam#reJoin(Player)} if the player was disconnected.
     *
     * @param player user to be added.
     */
    public void addPlayer(Player player) {
        if (!teammates.contains(player.getUniqueId())) teammates.add(player.getUniqueId());
        if (!teammatesCache.contains(player.getUniqueId())) teammatesCache.add(player.getUniqueId());
    }

    /**
     * Re-add a player to a team after he leaved the match.
     *
     * @param player target player.
     */
    public void reJoin(Player player) {
        if (!teammates.contains(player.getUniqueId())) teammates.add(player.getUniqueId());
        player.teleport(getSpawnPoint(), PlayerTeleportEvent.TeleportCause.PLUGIN);
        //todo clear inventory
        //todo give items and stuff
    }

    /**
     * Respawn a player.
     * Call this after managing your re-spawn screen at {@link org.bukkit.event.player.PlayerRespawnEvent}
     * or after your re-spawning screen.
     * The player will be re-spawned at the spawn point.
     *
     * @param player target player.
     */
    protected void respawn(Player player) {
        if (reSpawningScreen.containsKey(player.getUniqueId())) {
            reSpawningScreen.get(player.getUniqueId()).cancel();
            reSpawningScreen.remove(player.getUniqueId());
        }
        player.teleport(getSpawnPoint(), PlayerTeleportEvent.TeleportCause.PLUGIN);
        //todo un-hide
    }

    /**
     * This will hide the player and manage your own countdown task.
     * In case the game ended the task will be cancelled.
     * Call this at {@link org.bukkit.event.player.PlayerRespawnEvent}
     *
     * @param player target player.
     * @retun bukkit task id. NULL if the task was not created based on some conditions.
     */
    protected BukkitTask respawnScreen(Player player) {
        //todo hide player
        BukkitTask bt = Bukkit.getScheduler().runTaskTimer(PurpleMC.getOwner(), new Runnable() {
            int timer = 5;

            @Override
            public void run() {
                timer--;
                if (timer % 2 == 0 && timer > 10 || timer < 5 && timer > 0) {
                    //todo send title
                } else if (timer == 0) {
                    respawn(player);
                }
            }
        }, 0L, 20L);
        reSpawningScreen.put(player.getUniqueId(), bt);
        return bt;
    }

    /**
     * Set the team displayName
     *
     * @param displayName new display name.
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Get the team display name.
     *
     * @param player can be used for language systems. Use NULL if you are not using it.
     * @return display name.
     */
    public String getDisplayName(Player player) {
        return displayName;
    }
}
