package com.andrei1058.purplemc;

import com.andrei1058.purplemc.arenamanager.ArenaListener;
import com.andrei1058.purplemc.server.ServerType;
import com.andrei1058.purplemc.server.SetupListener;
import com.andrei1058.purplemc.worldmanager.MapAdapter;
import com.andrei1058.spigot.versionsupport.BlockSupport;
import com.andrei1058.spigot.versionsupport.ItemStackSupport;
import com.andrei1058.spigot.versionsupport.MaterialSupport;
import com.andrei1058.spigot.versionsupport.SoundSupport;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class PurpleMC {

    private static ItemStackSupport itemStackSupport = null;
    private static SoundSupport soundSupport = null;
    private static MaterialSupport materialSupport = null;
    private static BlockSupport blockSupport = null;
    private static MapAdapter mapAdapter = null;
    private static ServerType serverType;
    private static Plugin owner;

    private PurpleMC() {
    }

    /**
     * Use this in your onEnable method.
     * This will initialize the framework.
     *
     * @param plugin     your plugin instance.
     * @param mapAdapter custom adapter. Leave null if you don't want to use a custom adapter.
     * @param svType     server type.
     */
    public static void init(Plugin plugin, ServerType svType, MapAdapter mapAdapter) {
        Bukkit.getPluginManager().registerEvents(new ArenaListener(), plugin);
        Bukkit.getPluginManager().registerEvents(new SetupListener(), plugin);
        itemStackSupport = ItemStackSupport.SupportBuilder.load();
        soundSupport = SoundSupport.SupportBuilder.load();
        materialSupport = MaterialSupport.SupportBuilder.load();
        blockSupport = BlockSupport.SupportBuilder.load();
        serverType = svType;
        owner = plugin;

        if (mapAdapter == null) {
            if (Bukkit.getPluginManager().getPlugin("SlimeWorldManager") == null) {
                //todo
                //internal adapter
            } else {
                //todo
                //swm adapter
            }
        }
    }

    /**
     * @return multi version item stack support.
     */
    public static ItemStackSupport getItemStackSupport() {
        return itemStackSupport;
    }

    /**
     * @return multi version sound support.
     */
    public static SoundSupport getSoundSupport() {
        return soundSupport;
    }

    /**
     * @return multi version material support.
     */
    public static MaterialSupport getMaterialSupport() {
        return materialSupport;
    }

    /**
     * @return multi version block support.
     */
    public static BlockSupport getBlockSupport() {
        return blockSupport;
    }

    /**
     * @return map reset adapter.
     */
    public static MapAdapter getMapAdapter() {
        return mapAdapter;
    }

    /**
     * @return server type.
     */
    public static ServerType getServerType() {
        return serverType;
    }

    /**
     * @return the plugin using this framework.
     */
    public static Plugin getOwner() {
        return owner;
    }
}
